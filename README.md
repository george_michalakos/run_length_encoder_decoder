## Build

First **clone the repository**: `user@hostname:~$ git clone https://gitlab.com/george_michalakos/run_length_encoder_decoder.git`

Next execute the **build command** (You can replace the ' ~/bin/ ' with whatever directory you want): `user@hostname:~$ gcc --std=c99 run_length_encoder_decoder/rled.c -o ~/bin/rled`

Optionaly **remove the folder** with: `user@hostname:~$ rm -rf run_length_encoder_decoder`

## Manual

**Usage:** `rled [OPTIONS]... [INPUT FILE PATH] [OUTPUT FILE PATH]`

**Options:**
- `-d` : Decompress the input file instead of compressing it.
- `-s` : Show input and output file sizes.
- `-c` : Show the character and the character count thats ascociated with every run.
- `-h` : Display this message.
- `-1 -2 -4 -8` : Change the number of bytes used to store the character count for every run (The default value is 2). Make sure you use the same option when compressing and decompressing.

**Examples:**
- Compress file and show ouput and input file sizes : `rled -s file1.txt file1.comp`
- Crompress file and show run info and input/output file sizes : `rled -c -s file1.txt file1.compress`
- Decompress file : `rled -d file1.compress file1.txt`
- Decompress file and show and run info and input/output file sizes : `rled -d -s -c file1.compress file1.txt`

Tip:
You can combine options. For example `rled -d -s -c` is the same as `rled -dsc`
