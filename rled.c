#include <stdio.h>

// chr : Ο χαρακτήρας για του συγκεκριμένου run.
// cnt : Πόσες φορές εμφανίζεται ο συγκεκριμένος χαρακτήρας.
typedef struct run
{
    char chr;
    unsigned long cnt;
} Run;


// Η συγκεκριμένη συνάτριση συμπιέζει το αρχειό εισόδου inputFile με τη χρήση run lenght encoding. Το συμπιεζμένο αρχείο το αποθηκεύει στο outputFile 
int compressFile(FILE *inputFile, FILE *outputFile, char bytes, char chars)
{
    // Βρόχος που κάνει σειριακή προσπέλαση του αρχείου εισόδου. Σε κάθε επανάληψη διαβάζει έναν χαρακτήρα και τον αποθηκεύει στην μεταβλητή curChar.
    Run run = {run.chr = '\0', run.cnt = 0};
    unsigned long runNumber = 0;
    while(!feof(inputFile))
    {
        // Αν ο χαρακτήρας που διαβάσαμε δεν είναι ίδιος με τον χαρακτήρα του run, τότε αποθηκεύουμε τον run στο αρχείο εξόδου και ξεκινάμε έναν καινούριο. 
        // Διαφορέτικα, απλά αυξάνουμε το count του χαρακτήρα, αφού πρωτα ελεγχθεί αν αυτο θα προκαλέσει υπερχίληση για τον συγκεκριμένο αριθμό bytes.
        char curChar = fgetc(inputFile);
        if(curChar != run.chr)
        {
            // Ελένχουμε αν το count του run είναι 0. Αυτο γίνεται για να μην αποθηκεύσουμε το αρχικό \0.
            if(run.cnt)
            {
                // Αν για το συγκεκριμένο run υπάρχει μόνο ένας χαρακτήρας, τότε απλα τον γράφουμε στο αρχείο χωρής το πήθος.
                // Aν το count είναι πάνο απο 2 συν το πλήθος των bytes, τότε γράφουμε ένα \0 τον χαρακτήρα και το count του.
                // πχ. Αν run.chr == A και run.cnt == 2 τότε γράφουμε AA. Αν run.chr == B και run.cnt == 23 τότε γράφουμε \0B23.
                if(run.cnt > 2+bytes)
                {
                    fputc('\0',outputFile);
                    fputc(run.chr,outputFile);
                    fwrite(&run.cnt,bytes,1,outputFile);
                }
                else for(unsigned long i = 0; i < run.cnt; i++) fputc(run.chr,outputFile);

                // Εμφάνισε τον χαρακτήρα και το count του για το συγκεκριμένο run αν ο χρήστης έχει εισάγει τη παράμετρο -c.
                // Αν ο χαρακτήρας είναι new line ή carriage return ή tab δείξε \n \r \t αντοίστοιχα.
                if(chars)
                {
                    printf("- Run %lu -\n",++runNumber);
                    if(run.chr == 10) printf("Char: '\\n'\n");
                    else if(run.chr == 11) printf("Char: '\\t'\n");
                    else if(run.chr == 13) printf("Char: '\\r'\n");
                    else printf("Char: '%c'\n",run.chr);
                    printf("Count: %lu\n\n",run.cnt);
                }
            }
            run.chr = curChar;
            run.cnt = 1;
        }
        else if(bytes == 1 && run.cnt == 255UL) return 1;
        else if(bytes == 2 && run.cnt == 65535UL) return 1;
        else if(bytes == 4 && run.cnt == 4294967295UL) return 1;
        else if(bytes == 8 && run.cnt == 18446744073709551615UL) return 1;
        else run.cnt++;
    }
    return 0;
}


int decompressFile(FILE *inputFile, FILE *outputFile, char bytes, char chars)
{
    // Βρόχος που κάνει σειριακή προσπέλαση του αρχείου εισόδου. Σε κάθε επανάληψη διαβάζει έναν χαρακτήρα και τον αποθηκεύει στην μεταβλητή curChar.
    Run run = {run.chr = '\0', run.cnt = 0};
    unsigned long runNumber = 0;
    while(!feof(inputFile))
    {
        char curChar = fgetc(inputFile);
        if(curChar != run.chr)
        {
            if(run.cnt)
            {
                if(!run.chr)
                {
                    run.chr = curChar;
                    fread(&run.cnt,bytes,1,inputFile);
                    curChar = fgetc(inputFile);
                }
                for(unsigned long i = 0; i < run.cnt; i++) fputc(run.chr,outputFile);

                // Εμφάνισε τον χαρακτήρα και το count του για το συγκεκριμένο run αν ο χρήστης έχει εισάγει τη παράμετρο -c.
                // Αν ο χαρακτήρας είναι new line ή carriage return ή tab δείξε \n \r \t αντοίστοιχα.
                if(chars)
                {
                    printf("- Run %lu -\n",++runNumber);
                    if(run.chr == 10) printf("Char: '\\n'\n");
                    else if(run.chr == 11) printf("Char: '\\t'\n");
                    else if(run.chr == 13) printf("Char: '\\r'\n");
                    else  printf("Char: '%c'\n",run.chr);
                    printf("Count: %lu\n\n",run.cnt);
                }
            }
            run.chr = curChar;
            run.cnt = 1;
        }
        else if(bytes == 1 && run.cnt == 255UL) return 1;
        else if(bytes == 2 && run.cnt == 65535UL) return 1;
        else if(bytes == 4 && run.cnt == 4294967295UL) return 1;
        else if(bytes == 8 && run.cnt == 18446744073709551615UL) return 1;
        else run.cnt++;
    }
    return 0;
}


int main(int argc, char **argv)
{   
    // Ανάλυση των ορισμάτων που έδωσε ο χρίστης στο πρόγραμμα.
    // Το inputFilePath είναι ένας δεικτης στο όρισμα με το μονοπάτι στο αρχείο εισόδου. Αν είναι NULL ο χρήστης δεν εισήγαγε το μονοπάτι.
    // Το outputFilePath είναι ένας δεικτης στο όρισμα με το μονοπάτι στο αρχείο εξόδου.  Αν είναι NULL ο χρήστης δεν εισήγαγε το μονοπάτι.
    // Η μεταβλητή bytes είναι το count των bytes που θα χρησημοποιηθεί για την αποθήκευση του πλήθους των χαρακτήρων σε κάθε run.
    char *inputFilePath = NULL, *outputFilePath = NULL, bytes = 2, decompress = 0, size = 0, chars = 0, help = 0;
    for(unsigned long i = 1; i < argc; i++)
    {
        if(argv[i][0] == '-')
            for(unsigned long j = 1; argv[i][j]; j++)
                switch(argv[i][j])
                {
                    case 'd':
                        decompress = 1;
                    break;
                    case 's':
                        size = 1;
                    break;
                    case 'c':
                        chars = 1;
                    break;
                    case 'h':
                        help = 1;
                    break;
                    case '1':
                        bytes = 1;
                    break;
                    case '2':
                        bytes = 2;
                    break;
                    case '4':
                        bytes = 4;
                    break;
                    case '8':
                        bytes = 8;
                    break;
                }
        else if(!inputFilePath) inputFilePath = argv[i];
        else if(!outputFilePath) outputFilePath = argv[i];
    }

    // Έμφάνιση του μυνήματος βοήθειας όταν ο χρήστης χρησημοποιεί την παράμετρο -h ή όταν το inputFilePath ή το outputFilePath εχούν τιμή Null.
    if(help || !inputFilePath || !outputFilePath) 
    {
        printf("\nUsage: rled [OPTIONS]... [INPUT FILE PATH] [OUTPUT FILE PATH]\n\n");
        printf("Options:\n-d : Decompress the input file instead of compressing it.\n-s : Show input and output file sizes.\n-c : Show the character and the character count thats ascociated with every run.\n-h : Display this message.\n-1 -2 -4 -8 : Change the number of bytes used to store the character count for every run (The default value is 2). Make sure you use the same option when compressing and decompressing.\n\n");
        printf("Examples:\nCompress file and show ouput and input file sizes : 'rled -s file1.txt file1.comp'\nCrompress file and show run info and input/output file sizes : 'rled -c -s file1.txt file1.compress'\nDecompress file : 'rled -d file1.compress file1.txt'\nDecompress file and show and run info and input/output file sizes : 'rled -d -s -c file1.compress file1.txt'\n\n");
        printf("Tip:\nYou can combine options. For example 'rled -d -s -c' is the same as 'rled -dsc'\n\n");
        return 0;
    }

    // Άνοιγμα του αρχείου εισόδου.
    FILE *inputFile = fopen(inputFilePath,"r");
    if(!inputFile)
    {
        printf("Couldn't read the input file '%s'.\n", inputFilePath);
        return 1;
    }

    // Άνοιγμα του αρχείου εξόδου.
    FILE *outputFile = fopen(outputFilePath,"w");
    if(!outputFile)
    {
        printf("Couldn't create the output file '%s'.\n", outputFilePath);
        return 2;
    }

    // Συμπίεσε ή αποσυμπίεσε το αρχείο είσόδου με βάση το αν ο χρήστης έχει εισάγει την παράμετρο -d.
    // Επίσης επέστρεψε 3 αν γίνει κάποιο λάθος κατα τη διάρκεια της συμπίεσης ή της αποσυμπίεσης.
    if(decompress && decompressFile(inputFile,outputFile,bytes,chars) || !decompress && compressFile(inputFile,outputFile,bytes,chars)) return 3;
    
    // Εμφάνισε το μέγεθος του αρχείου εισόδου και του αρχείου εξόδου, όταν ο χρήστης έχει εισάγει την παράμετρο -s.
    if(size) printf("Input file size: %lu bytes.\nOutput file size: %ld bytes.\n",ftell(inputFile),ftell(outputFile));
    fclose(inputFile);
    fclose(outputFile);
    return 0;
}
